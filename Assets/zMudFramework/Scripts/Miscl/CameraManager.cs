﻿using UnityEngine;
using System.Collections;

namespace MUD
{
    public class CameraManager : MonoBehaviour
    {
        public Camera _Camera;
        public float _AspectRatioX = 16.0f;
        public float _AspectRatioY = 10.0f;

        void Start()
        {
            float targetaspect = _AspectRatioX / _AspectRatioY;

            float windowaspect = (float)Screen.width / (float)Screen.height;

            float scaleheight = windowaspect / targetaspect;

            if (scaleheight < 1.0f)
            {
                Rect rect = _Camera.rect;

                rect.width = 1.0f;
                rect.height = scaleheight;
                rect.x = 0;
                rect.y = (1.0f - scaleheight) / 2.0f;

                _Camera.rect = rect;
            }
            //else 
            //{
            //    float scalewidth = 1.0f / scaleheight;

            //    Rect rect = _Camera.rect;

            //    rect.width = scalewidth;
            //    rect.height = 1.0f;
            //    rect.x = (1.0f - scalewidth) / 2.0f;
            //    rect.y = 0;

            //    _Camera.rect = rect;
            //}
        }


    }
}