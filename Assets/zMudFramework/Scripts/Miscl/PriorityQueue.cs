﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MUD
{
    public class PriorityQueue<TData>
    {
        /// <summary>
        /// Tkey:Int = Priority;
        /// TValue:List<Int> = KeyCollection
        /// </summary>
        private SortedDictionary<int, List<int>> mPriorityKeyCollection;
        /// <summary>
        /// TKey:Int = Key
        /// TData:TData = Data
        /// </summary>
        private Dictionary<int, TData> mKeyDataCollection;
        private int mDefaultPriority = 0;
        public PriorityQueue(int DefaultPriority)
        {
            mDefaultPriority = DefaultPriority;
        }

        public PriorityQueue() { }

        public TData this[int Key]
        {
            get
            {
                return mKeyDataCollection[Key];
            }
        }
        public bool Add(int Key, TData item, int priority = -1)
        {
            if (mPriorityKeyCollection == null)
                mPriorityKeyCollection = new SortedDictionary<int, List<int>>();
            if (mKeyDataCollection == null)
                mKeyDataCollection = new Dictionary<int, TData>();

            if (priority == -1) priority = mDefaultPriority;

            if (!mKeyDataCollection.ContainsKey(Key))
                mKeyDataCollection.Add(Key, item);
            else
                return false;

            if (mPriorityKeyCollection.ContainsKey(priority))
            {
                if (mPriorityKeyCollection[priority] == null)
                    mPriorityKeyCollection[priority] = new List<int>();
                mPriorityKeyCollection[priority].Add(Key);
            }
            else
            {
                mPriorityKeyCollection.Add(priority, new List<int>() { Key });
            }
            return true;
        }

        //public TData[] this[int Priority]
        //{
        //    get
        //    {
        //        if (mKeyDataCollection == null || mPriorityKeyCollection == null)
        //            return null;
        //        TData[] dataCollection;
        //        foreach (var item in mPriorityKeyCollection[Priority]) {
        //            dataCollection[i] = mKeyDataCollection[mPriorityKeyCollection[Priority][i]];
        //        }
        //        return dataCollection;
        //    }
        //}

        public TData GetFirstItem()
        {
            if (mKeyDataCollection == null || mPriorityKeyCollection == null)
                return default(TData);
            return mKeyDataCollection[mPriorityKeyCollection.First().Value.First()];
        }

        public TData GetLastItem()
        {
            if (mKeyDataCollection == null || mPriorityKeyCollection == null)
                return default(TData);
            return mKeyDataCollection[mPriorityKeyCollection.Last().Value.First()];
        }

        public TData[] GetAllItemsOf(int Priority)
        {
            if (mKeyDataCollection == null || mPriorityKeyCollection == null)
                return null;

            TData[] dataCollection = null;
            for (int i = 0; i < mPriorityKeyCollection[Priority].Count; i++)
            {
                dataCollection[i] = mKeyDataCollection[mPriorityKeyCollection[Priority][i]];
            }
            return dataCollection;
        }

        public bool Remove(int key)
        {
            if (mKeyDataCollection == null || mPriorityKeyCollection == null)
                return false;
            if (mKeyDataCollection.ContainsKey(key))
                mKeyDataCollection.Remove(key);
            else
                return false;

            return mPriorityKeyCollection.Values.FirstOrDefault((x) => x.Contains(key)).Remove(key);
        }
    }
}