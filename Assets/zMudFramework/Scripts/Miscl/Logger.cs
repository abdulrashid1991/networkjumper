﻿using UnityEngine;
using System.Collections;

namespace MUD
{
    public class Logger
    {
        public static void LogError(string inData, int priority = 10)
        {
            Debug.LogError(inData);
        }

        public static void LogException(System.Exception inException, int priority = 10)
        {
            Debug.LogException(inException);
        }

        public static void Log(object inData, int priority = 10)
        {
            Debug.Log(inData.ToString());
        }

        public static void LogLinkMissng(string inData, int priority = 10)
        {
            Debug.Log(inData);
        }
    }
}