﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameStateCollection;

/*Enum.GetUnderlyingType(typeof(Type))  -- Try Implementing using this*/
public interface IStateListner
{
    void OnStateChanged<T>(StateType type, T state);
}
namespace MUD.NotificationCenter
{
    public abstract class StateManager
    {

        private Dictionary<StateType, List<IStateListner>> mGameStateCollection;
        #region StaticFunctions
        public virtual void AddObserver(StateType type, IStateListner component)
        {
            if (mGameStateCollection == null)
                mGameStateCollection = new Dictionary<StateType, List<IStateListner>>();

            List<IStateListner> tempList;
            if (mGameStateCollection.TryGetValue(type, out tempList))
            {
                tempList.Add(component);
                return;
            }
            mGameStateCollection.Add(type, new List<IStateListner>() { component });
        }
        public abstract void SetState<T>(T state);
        #endregion

        #region LocalFunctions
        protected void SetState<T>(T state, StateType type)
        {
            List<IStateListner> mObserverList;
            if (mGameStateCollection.TryGetValue(type, out mObserverList))
            {
                for (int j = 0; j < mObserverList.Count; j++)
                {
                    mObserverList[j].OnStateChanged<T>(type, state);
                }
            }
        }
        protected T ConvertToState<T>(string state)
        {
            return (T)Enum.Parse(typeof(T), state, false);
        }
        private void DisplayCollection()
        {
            foreach (var key in mGameStateCollection.Keys)
            {
                List<IStateListner> temp;
                if (mGameStateCollection.TryGetValue(key, out temp))
                {
                    for (int i = 0; i < temp.Count; i++)
                    {
                        Debug.Log("StateType : " + key.ToString() + "Component Name : " + temp[i].ToString());
                    }
                }
            }
        }
        #endregion
    }
}