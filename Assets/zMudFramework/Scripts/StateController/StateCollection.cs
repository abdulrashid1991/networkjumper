﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameStateCollection;

namespace GameStateCollection
{
    public enum StateType { ApplicationState, GameState, ScreenState }
    public enum ApplicationState { None, Start, End }
    public enum GameState { None, Start, End, isRunning, isPaused }
    public enum ScreenState { None, Start, End }
}

namespace MUD.NotificationCenter
{
    public class GameStateManager : StateManager
    {
        private static GameStateManager mInstance;
        public static GameStateManager pInstance
        {
            get
            {
                if (mInstance != null)
                    return mInstance;
                else
                    return null;
                ///~ Log Error
            }
        }

        private ApplicationState mApplicationState = ApplicationState.None;
        private GameState mGameState = GameState.None;
        private ScreenState mScreenState = ScreenState.None;

        public ApplicationState pApplicationState
        {
            get { return mApplicationState; }
        }
        public GameState pGameState
        {
            get { return mGameState; }
        }
        public ScreenState pScreenState
        {
            get { return mScreenState; }
        }

        void Awake()
        {
            Init();
        }
        public void Init()
        {
            mInstance = this;
        }
        public override void AddObserver(StateType type, IStateListner component)
        {
            base.AddObserver(type, component);
        }
        public override void SetState<T>(T state)
        {
            if (mApplicationState.GetType() == typeof(T))
            {
                mApplicationState = ConvertToState<ApplicationState>(state.ToString());
                SetState<ApplicationState>(mApplicationState, StateType.ApplicationState);
            }
            else if (mGameState.GetType() == typeof(T))
            {
                mGameState = ConvertToState<GameState>(state.ToString());
                SetState<GameState>(mGameState, StateType.GameState);
            }
            else if (mScreenState.GetType() == typeof(T))
            {
                mScreenState = ConvertToState<ScreenState>(state.ToString());
                SetState<ScreenState>(mScreenState, StateType.ScreenState);
            }
        }
    }
}