﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MUD.NotificationCenter
{
    public class NotificationManager : MonoBehaviour
    {
        private Hashtable notifications = new Hashtable();
        public virtual void AddObserver(Component observer, string name, Component sender)
        {
            if (string.IsNullOrEmpty(name)) { Debug.Log("Null name specified for notification in AddObserver."); return; }
            if (notifications[name] == null)
            {
                notifications[name] = new List<Component>();
            }

            List<Component> notifyList = notifications[name] as List<Component>;

            if (!notifyList.Contains(observer)) { notifyList.Add(observer); }
        }
        public virtual void RemoveObserver(Component observer, string name)
        {
            List<Component> notifyList = (List<Component>)notifications[name];

            if (notifyList != null)
            {
                if (notifyList.Contains(observer)) { notifyList.Remove(observer); }
                if (notifyList.Count == 0) { notifications.Remove(name); }
            }
        }
        public virtual void PostNotification<T>(Notification<T> aNotification)
        {
            if (string.IsNullOrEmpty(aNotification.name))
            {
                Debug.Log("Null name sent to PostNotification.");
                return;
            }
            List<Component> notifyList = (List<Component>)notifications[aNotification.name];
            if (notifyList == null)
            {
                Debug.Log("Notify list not found in PostNotification: " + aNotification.name);
                return;
            }

            List<Component> observersToRemove = new List<Component>();

            foreach (Component observer in notifyList)
            {
                if (!observer) { observersToRemove.Add(observer); }
                else
                {
                    observer.SendMessage(aNotification.name, aNotification.data, SendMessageOptions.DontRequireReceiver);
                }
            }

            foreach (Component observer in observersToRemove)
            {
                notifyList.Remove(observer);
            }
        }
        public virtual void PostNotification(Notification aNotification)
        {
            if (string.IsNullOrEmpty(aNotification.name))
            {
                Debug.Log("Null name sent to PostNotification.");
                return;
            }
            List<Component> notifyList = (List<Component>)notifications[aNotification.name];
            if (notifyList == null)
            {
                Debug.Log("Notify list not found in PostNotification: " + aNotification.name);
                return;
            }

            List<Component> observersToRemove = new List<Component>();

            foreach (Component observer in notifyList)
            {
                if (!observer) { observersToRemove.Add(observer); }
                else
                {
                    observer.SendMessage(aNotification.name, SendMessageOptions.DontRequireReceiver);
                }
            }

            foreach (Component observer in observersToRemove)
            {
                notifyList.Remove(observer);
            }
        }
        public class Notification<T>
        {
            public Component sender;
            public string name;
            public T data;

            public Notification(Component aSender, string aName) { sender = aSender; name = aName; data = default(T); }
            public Notification(Component aSender, string aName, T aData) { sender = aSender; name = aName; data = aData; }
        }
        public class Notification
        {
            public Component sender;
            public string name;

            public Notification(Component aSender, string aName) { sender = aSender; name = aName; }
        }
    }
}