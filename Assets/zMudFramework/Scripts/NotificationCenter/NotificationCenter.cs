﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MUD.NotificationCenter
{
    public class NotificationCenter : NotificationManager
    {
        private static NotificationCenter mDefNotifier;
        public static NotificationCenter pDefNotifier
        {
            get
            {
                if (mDefNotifier == null)
                {
                    GameObject mdefNotifierGO = new GameObject("DefNotifier");
                    mDefNotifier = mdefNotifierGO.AddComponent<NotificationCenter>();

                }
                return mDefNotifier;
            }
        }

        public void AddObserver(Component observer, string name) { AddObserver(observer, name, null); }
        public void PostNotification(Component aSender, string aName) { PostNotification(new Notification(aSender, aName)); }
        public void PostNotification<T>(Component aSender, string aName, T aData) { PostNotification<T>(new Notification<T>(aSender, aName, aData)); }
        public override void PostNotification(NotificationManager.Notification aNotification)
        {
            base.PostNotification(aNotification);
        }
        public override void AddObserver(Component observer, string name, Component sender)
        {
            base.AddObserver(observer, name, sender);
        }
        public override void RemoveObserver(Component observer, string name)
        {
            base.RemoveObserver(observer, name);
        }
        public override void PostNotification<T>(Notification<T> aNotification)
        {
            base.PostNotification<T>(aNotification);
        }
    }
}