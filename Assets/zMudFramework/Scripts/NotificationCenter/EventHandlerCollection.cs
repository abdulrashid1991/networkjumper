﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

namespace MUD
{
    [Serializable]
    public class GenericEvent : UnityEvent
    { }

    [Serializable]
    public class TriggerEvent : UnityEvent<Collider, GameObject>
    { }

    [Serializable]
    public class TouchEvent : UnityEvent<object, object>
    { }
}