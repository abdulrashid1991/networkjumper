﻿using UnityEngine;
using System.Collections;

public enum Direction { Forward, Backward }
public enum Axis { X, Y }
namespace MUD.World
{
    public enum TileState { Instantiated, Foreground, Background }
    [System.Serializable]
    public class TileLayerData
    {
        public GameObject[] TilePrefabs;
        public Transform LayerTransform;
        public int MaxObjects;
        public float SpeedFactor;
        public Axis Axis;
        public int Layer;
        public float TileSize;
        public float TileInitPos;
        public float Offset;
        private int mCurrIndex = 0;

        [HideInInspector]
        public float Speed {
            get {
                return WorldGenerator.WorldSpeed == 0 ? 0 : (WorldGenerator.WorldSpeed * SpeedFactor) * Time.deltaTime * (Axis == Axis.X ? -1 : 1);
            }
        }

        [HideInInspector]
        public TileLayer mLayerInstance;

        [HideInInspector]
        public GameObject CurrTilePrefab {
            get {
                if (TilePrefabs != null && TilePrefabs.Length > 0)
                {
                    if (TilePrefabs.Length > mCurrIndex)
                    {
                        return TilePrefabs[mCurrIndex++];
                    }
                    else
                    {
                        mCurrIndex = 0;
                        return TilePrefabs[mCurrIndex];
                    }
                }
                else
                {
                    mCurrIndex = 0;
                    return null;
                }
                
            }
        }

        [HideInInspector]
        public float FirstItemPosition {
            get {
                return -1 * ((Mathf.Abs(FirstItemIndex) * TileSize) + TileInitPos);
            }
        }

        [HideInInspector]
        public int FirstItemIndex
        {
            get
            {
                return -1 * (Mathf.FloorToInt(Mathf.Abs(MaxObjects / 2) - 1));
            }
        }

        [HideInInspector]
        public int LastItemIndex
        {
            get
            {
                return (Mathf.FloorToInt(Mathf.Abs(MaxObjects / 2) - 1));
            }
        }
    }

    public static class Tag { 
        public static readonly string World_Full_Collider = "ScreenFull";
        public static readonly string Tile_Middle_Trigger = "TileMiddle";
    }
    public class WorldGenerator : MonoBehaviour
    {
        public float _speed;

        public static float WorldSpeed;

        void Start() {
            WorldSpeed = _speed;
        }
        void Update() {
        }

    }
}