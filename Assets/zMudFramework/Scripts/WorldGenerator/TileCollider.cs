﻿using UnityEngine;
using System.Collections;
using MUD;

namespace MUD.World
{
    public class TileCollider : MonoBehaviour
    {


        public TriggerEvent _OnTriggerEnter = new TriggerEvent();
        public TriggerEvent _OnTriggerStay = new TriggerEvent();
        public TriggerEvent _OnTriggerExit = new TriggerEvent();



        void OnTriggerEnter(Collider collider)
        {
            //Debug.LogError("[TileCollider] Enter" + collider.gameObject.name + " || This" + this.gameObject.name);
            _OnTriggerEnter.Invoke(collider, this.gameObject);
        }

        void OnTriggerExit(Collider collider)
        {
            //Debug.LogError("[TileCollider] Exit" + collider.gameObject.name + " || This" + this.gameObject.name);
            _OnTriggerExit.Invoke(collider, this.gameObject);
        }

        void OnTriggerStay(Collider collider)
        {
            //Debug.LogError("[TileCollider] Stay" + collider.gameObject.name + " || This" + this.gameObject.name);
            _OnTriggerStay.Invoke(collider, this.gameObject);
        }
    }
}