﻿using UnityEngine;
using System.Collections;

namespace MUD.World
{
    public class TileObject : MonoBehaviour{

        [HideInInspector]
        public TileObject mInstance;
        private TileLayerData mLayerData;
        private Vector3 mTranslationVector;

        private bool mFwdEntered, mBwdEntered;
        public static TileObject Init(TileLayerData data, Vector3 position) {
            GameObject thisPrefab = data.CurrTilePrefab;
            GameObject obj = (GameObject)Instantiate(thisPrefab, new Vector3(position.x, position.y, data.LayerTransform.position.z), thisPrefab.transform.rotation);
            obj.transform.parent = data.LayerTransform;
            obj.name = "Tile";
            TileObject objData = obj.GetComponent<TileObject>();
            objData.mInstance = objData;
            objData.mLayerData = data;
            return objData;
        }

        void Start() {
            mFwdEntered = mBwdEntered = false;
        }

        void Update() {
            if (mLayerData == null)
                //Destroy GameObject
                return;

            switch (mLayerData.Axis)
            {
                case Axis.X:
                    mTranslationVector.x = mLayerData.Speed;// * (mLayerData.Direction == Direction.Forward ? 1 : -1);
                    break;
                case Axis.Y:
                    mTranslationVector.y = mLayerData.Speed;// * (mLayerData.Direction == Direction.Forward ? -1 : 1);
                    break;
            }
            transform.position += mTranslationVector;
        }

        public bool DestroyTile() {
            Destroy(this.gameObject);
            return true;
        }

        public void TriggerExit(Collider worldCollider, GameObject tileObject)
        {
            if (tileObject.tag == Tag.Tile_Middle_Trigger && worldCollider.tag == Tag.World_Full_Collider)
            {
                if (mLayerData.Speed > 0)
                {
                    mLayerData.mLayerInstance.InitBwdTile();
                }
                else
                {
                    mLayerData.mLayerInstance.InitFwdTile();
                }
            }
        }
    }
}
