﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MUD.World
{
    public class TileLayer : MonoBehaviour{
        public TileLayerData _ThisData;
        private List<TileObject> mTileObjectQueue;

        private Vector3 mNextFwdInitPos;
        private Vector3 mNextBwdInitPos;

        void Awake() {
            InitTileData();
        }

        void Start() {

        }

        private void InitTileData()
        {
            _ThisData.mLayerInstance = this;
            mTileObjectQueue = new List<TileObject>();
            mNextFwdInitPos = Vector3.zero;
            mNextBwdInitPos = Vector3.zero;
            switch (_ThisData.Axis)
            {
                case Axis.X:
                    mNextFwdInitPos.x = _ThisData.FirstItemPosition;
                    mNextFwdInitPos.y = _ThisData.Offset;

                    mNextBwdInitPos.x = _ThisData.FirstItemPosition;
                    mNextBwdInitPos.y = _ThisData.Offset;
                    break;
                case Axis.Y:
                    mNextFwdInitPos.y = _ThisData.FirstItemPosition;
                    mNextFwdInitPos.x = _ThisData.Offset;

                    mNextBwdInitPos.y = _ThisData.FirstItemPosition;
                    mNextBwdInitPos.x = _ThisData.Offset;
                    break;
            }

            for (int i = 0; i < _ThisData.MaxObjects; i++)
            {
                TileObject tileData = TileObject.Init(_ThisData, mNextFwdInitPos);
                mTileObjectQueue.Add(tileData);
                switch (_ThisData.Axis)
                {
                    case Axis.X:
                        mNextFwdInitPos.x += _ThisData.TileSize;
                        break;
                    case Axis.Y:
                        mNextFwdInitPos.y += _ThisData.TileSize;
                        break;
                }
            }
        }

        void Update() { 
            if (Input.GetKeyDown(KeyCode.F))
            {
                InitFwdTile();
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                InitBwdTile();
            }
        }
        void FixedUpdate() {
            switch (_ThisData.Axis)
            {
                case Axis.X:
                    mNextFwdInitPos.x += _ThisData.Speed;
                    mNextBwdInitPos.x += _ThisData.Speed;
                    break;
                case Axis.Y:
                    mNextFwdInitPos.y += _ThisData.Speed;
                    mNextBwdInitPos.y += _ThisData.Speed;
                    break;
            }

        }

        public TileObject AddTileItem(Direction direction) {
            TileObject tileData = new TileObject();
            switch (direction)
            {
                case Direction.Forward:
                    tileData = TileObject.Init(_ThisData, mNextFwdInitPos);
                    switch (_ThisData.Axis)
                    {
                        case Axis.X:
                            mNextFwdInitPos.x += _ThisData.TileSize;
                            break;
                        case Axis.Y:
                            mNextFwdInitPos.y += _ThisData.TileSize;
                            break;
                    }
                    break;
                case Direction.Backward:
                    tileData = TileObject.Init(_ThisData, mNextBwdInitPos);
                    switch (_ThisData.Axis)
                    {
                        case Axis.X:
                            mNextBwdInitPos.x -= _ThisData.TileSize;
                            break;
                        case Axis.Y:
                            mNextBwdInitPos.y -= _ThisData.TileSize;
                            break;
                    }
                    break;
            }
            return tileData;
        }

        private void RemoveTileItem(Direction direction) {
            switch (direction)
            {
                case Direction.Forward:
                    TileObject lastTile = mTileObjectQueue.First();
                    lastTile.DestroyTile();
                    mTileObjectQueue.Remove(lastTile);
                    switch (_ThisData.Axis)
                    {
                        case Axis.X:
                            mNextBwdInitPos.x += _ThisData.TileSize;
                            break;
                        case Axis.Y:
                            mNextBwdInitPos.y += _ThisData.TileSize;
                            break;
                    }
                    break;
                case Direction.Backward:
                    TileObject firstTile = mTileObjectQueue.Last();
                    firstTile.DestroyTile();
                    mTileObjectQueue.Remove(firstTile);
                    switch (_ThisData.Axis)
                    {
                        case Axis.X:
                            mNextFwdInitPos.x -= _ThisData.TileSize;
                            break;
                        case Axis.Y:
                            mNextFwdInitPos.y -= _ThisData.TileSize;
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        //private TileObject InitFwdTile(){
        //    TileObject tileData = TileObject.Init(_ThisData, new Vector3());
        //    mTileObjectQueue.Add(tileData.gameObject.GetInstanceID(), tileData);
        //    switch (_ThisData.Axis)
        //    {
        //        case Axis.X:
        //            //_ThisData.NextInitPos.x += _ThisData.TileSize * (_ThisData.Direction == Direction.Forward ? -1 : 1);
        //            break;
        //        case Axis.Y:
        //            //_ThisData.NextInitPos.y += _ThisData.TileSize * (_ThisData.Direction == Direction.Forward ? 1 : -1);
        //            break;
        //    }
        //    return tileData;
        //}

        public void InitFwdTile() {
            RemoveTileItem(Direction.Forward);
            mTileObjectQueue.Add(AddTileItem(Direction.Forward));
        }

        public void InitBwdTile() {
            RemoveTileItem(Direction.Backward);
            mTileObjectQueue.Insert(0, AddTileItem(Direction.Backward));

        }
    }
}