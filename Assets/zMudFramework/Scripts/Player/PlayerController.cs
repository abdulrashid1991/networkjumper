﻿using UnityEngine;
using System.Collections;
using MUD;
using MUD.World;

namespace MUD.PlayerController
{
    public struct PlayerInfo
    {
        public GameObject PlayerGO;
        public Animator PlayerAnimator;
    }

    public class PlayerController : MonoBehaviour
    {
        public GameObject _PlayerGO;
        public Axis _MovmentAxis;

        public float _MaxJumpHeight;
        public float _JumpSpeed;

        public float _MaxWalkSpeed;
        private float mPlayerDefPos;
        public PlayerInfo _PlayerInfo;

        void Start()
        {
            if (_MovmentAxis == Axis.X)
            {
                mPlayerDefPos = _PlayerGO.transform.position.y;
            }
            else if (_MovmentAxis == Axis.Y)
            {
                mPlayerDefPos = _PlayerGO.transform.position.x;
            }
        }


        void Update()
        {

        }

        public void OnUserInput(object moveDelta, object tapcount){
            if (((float)moveDelta) > 0)
            {
                if (WorldGenerator.WorldSpeed < _MaxWalkSpeed)
                    WorldGenerator.WorldSpeed += 1;
            }
            else if (((float)moveDelta) < 0)
            {
                if (WorldGenerator.WorldSpeed > (-1 * _MaxWalkSpeed))
                    WorldGenerator.WorldSpeed -= 1;
            }
            else
            {
                WorldGenerator.WorldSpeed = 0;
            }

            if ((int)tapcount == 2)
                WorldGenerator.WorldSpeed = 0;
        }

        public void OnJump(object isPressed, object holdTime)
        {
            if ((bool)isPressed) {
                StopCoroutine("Jump");
                StartCoroutine("Jump");
            }
        }

        IEnumerator Jump() {
            float jumpdelta =  _JumpSpeed * Time.deltaTime;
            while (_PlayerGO.transform.position.y  <= mPlayerDefPos + _MaxJumpHeight)
            {
                _PlayerGO.transform.Translate(_MovmentAxis == Axis.X ? new Vector3(0, jumpdelta, 0) : new Vector3(jumpdelta, 0, 0));
                yield return null;
            }
            while (_PlayerGO.transform.position.y >= mPlayerDefPos )
            {
                _PlayerGO.transform.Translate(_MovmentAxis == Axis.X ? new Vector3(0, -jumpdelta, 0) : new Vector3(-jumpdelta, 0, 0));
                yield return null;
            }
        }
    }
}