﻿using UnityEngine;
using System.Collections;
using GameStateCollection;
using MUD.NotificationCenter;
namespace MUD
{
    public class MUDMonoBase : MonoBehaviour, IStateListner
    {

        protected bool isGamePause = false;

        void Start()
        {
            GameStateManager.pInstance.AddObserver(StateType.GameState, this);
        }

        public void OnStateChanged<T>(GameStateCollection.StateType type, T state)
        {
            if (type == StateType.GameState)
            {
                if (state.ToString() == GameState.isPaused.ToString())
                    isGamePause = true;
                else
                    isGamePause = false;
//Test Line
            }
        }
    }
}