﻿using UnityEngine;
using System.Collections;
using MUD;

namespace MUD.InputManager
{
    public class NavigationHandler : InputHandler
    {

        private InputItem _ThisData;

        private float mInputDelta;
        private float pInputDelta
        {
            get
            {
                return mInputDelta;
            }
            set
            {
                mInputDelta = value;
                TriggerTouchEvent(mInputDelta);
            }
        }

        private int mLatchedFingerId = -1;
        private Touch mCurrentTouchData;
        private Touch? pCurrentTouchData
        {
            get
            {
                if (mLatchedFingerId == -1)
                    return null;
                else
                    return (Touch)mCurrentTouchData;
            }
            set
            {
                if (value == null)
                {
                    mLatchedFingerId = -1;
                }
                else
                {
                    mCurrentTouchData = (Touch)value;
                    mLatchedFingerId = mCurrentTouchData.fingerId;
                }
            }
        }

        private Vector2 mFirstTouchPoint;
        private Vector2 mCurrTouchPoint;
        public override void Init(InputItem data)
        {
            _ThisData = data;
        }

        public override bool IsFingerDown()
        {
            if (pCurrentTouchData != null)
                return true;
            else
                return false;
        }
        void Update()
        {
            if (Input.touches.Length == 0){
                //Reset();
                return;
            }

            if (pCurrentTouchData == null)
            {
                for (int i = 0; i < Input.touches.Length; i++)
                {
                    if (IsValidTouch(Input.touches[i]))
                    {
                        pCurrentTouchData = Input.touches[i];
                        mFirstTouchPoint = ((Touch)pCurrentTouchData).position;
                    }
                }

            }
            else
            {
                for (int i = 0; i < Input.touches.Length; i++)
                {
                    if (((Touch)pCurrentTouchData).fingerId == Input.touches[i].fingerId)
                    {
                        pCurrentTouchData = Input.touches[i];
                        break;
                    }
                }
            }
            if (pCurrentTouchData == null){
                Reset();
                return;
            }
            switch (((Touch)pCurrentTouchData).phase)
            {
                case TouchPhase.Began:
                    mFirstTouchPoint = ((Touch)pCurrentTouchData).position;
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    Reset();
                    break;
                case TouchPhase.Stationary:
                case TouchPhase.Moved:
                    mCurrTouchPoint = ((Touch)pCurrentTouchData).position;
                    switch (_ThisData.Axis)
                    {
                        case Axis.X:
                            pInputDelta = mCurrTouchPoint.x - mFirstTouchPoint.x;
                            break;
                        case Axis.Y:
                            pInputDelta = mCurrTouchPoint.y - mFirstTouchPoint.y;
                            break;
                        default:
                            break;
                    }
                    break;
            }

        }

        void TriggerTouchEvent(float mMovmentDelta)
        {
            if (_ThisData.Reciver != null)
                _ThisData.Reciver.Invoke(mMovmentDelta,pCurrentTouchData != null? ((Touch)pCurrentTouchData).tapCount : 0);
        }

        public override void Reset()
        {
            //InputManager.ActiveTouchInfo.Remove(mLatchedFingerId);
            mCurrTouchPoint = mFirstTouchPoint = Vector2.zero;
            pCurrentTouchData = null;
            pInputDelta = 0;
        }

        public override void DeInit()
        {

        }

        bool IsValidTouch(Touch currTouch) {
            if(InputManager.InputHandlerColl == null)
                return true;
            Ray ray = Camera.main.ScreenPointToRay(currTouch.position);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit.collider != null)
            {
                for (int i = 0; i < InputManager.InputHandlerColl.Length; i++)
                {
                    if (InputManager.InputHandlerColl[i].Type == InputType.Button || InputManager.InputHandlerColl[i].Type == InputType.Joystick)
                    {
                        if (InputManager.InputHandlerColl[i].Handler.gameObject.GetInstanceID() == hit.collider.gameObject.GetInstanceID())
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}