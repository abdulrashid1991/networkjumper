﻿using UnityEngine;
using System.Collections;
using MUD.InputManager;
public class ButtonHandler: InputHandler {

    private InputItem mThisData;

    private float mHoldTime = 0;
    private bool mIsButtonClicked;

    private bool pIsButtonClicked {
        get {
            return mIsButtonClicked;
        }
        set {
            mIsButtonClicked = value;
            if (mIsButtonClicked)
            {
                TriggerClickEvent(mIsButtonClicked, mHoldTime);
            }
        }
    }

    private void TriggerClickEvent(bool mIsButtonClicked, float mHoldTime)
    {
        if (mThisData != null && mThisData.Reciver != null)
        {
            mThisData.Reciver.Invoke(mIsButtonClicked, mHoldTime);            
        }
    }

    public override void Init(InputItem data)
    {
        mThisData = data;
    }


    void Update() {
        if (Input.touches.Length == 0)
            return;
        

        for (int i = 0; i < Input.touches.Length; i++)
        {
            if (IsValidTouch(Input.touches[i]))
            {
                //if (InputManager.ActiveTouchInfo.ContainsKey(Input.touches[i].fingerId))
                //{
                //}
                //else
                //{
                //    InputManager.ActiveTouchInfo.Add(Input.touches[i].fingerId, Input.touches[i]);
                //}
                pIsButtonClicked = true;
                mHoldTime += Time.deltaTime;
                return;
            }
        }
        Reset();
    }

    public override bool IsFingerDown()
    {
        return pIsButtonClicked;
    }

    public override void Reset()
    {

        pIsButtonClicked = false;
        mHoldTime = 0;
    }

    public override void DeInit()
    {
        Reset();
    }

    bool IsValidTouch(Touch currTouch)
    {
        Ray ray = Camera.main.ScreenPointToRay(currTouch.position);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.GetInstanceID() == this.gameObject.GetInstanceID())
            {
                return true;
            }
        }
        return false;
    }
}
