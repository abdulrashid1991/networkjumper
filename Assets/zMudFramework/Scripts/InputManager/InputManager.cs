﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MUD;

namespace MUD.InputManager
{
    public enum InputType { Joystick, NavigationControl, Button };

    [System.Serializable]
    public class InputItem
    {
        public InputType Type;
        public InputHandler Handler;

        [Header("Settings")]
        public Axis Axis;
        public TouchEvent Reciver = new TouchEvent();
    }

    public class InputManager : MonoBehaviour
    {

        public bool _SimulateWithMouse;
        public InputItem[] _InputHandlerColl;

        public static Dictionary<int, Touch> ActiveTouchInfo;
        public static InputItem[] InputHandlerColl;
        void Awake()
        {
            ActiveTouchInfo = new Dictionary<int, Touch>();
            InputHandlerColl = _InputHandlerColl;
        }

        void Start()
        {
            Input.simulateMouseWithTouches = _SimulateWithMouse;
            for (int i = 0; i < _InputHandlerColl.Length; i++)
            {
                if (_InputHandlerColl[i].Handler != null)
                    _InputHandlerColl[i].Handler.Init(_InputHandlerColl[i]);
            }
        }

        void Update()
        {
        }

        void OnDestroy()
        {

        }

        void OnEnable()
        {

        }

        void OnDisable()
        {

        }
    }
}