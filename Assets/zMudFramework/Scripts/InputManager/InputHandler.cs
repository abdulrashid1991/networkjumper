﻿using UnityEngine;
using System.Collections;

namespace MUD.InputManager
{
    public abstract class InputHandler : MonoBehaviour
    {
        public abstract void Init(InputItem data);
        public abstract bool IsFingerDown();
        public abstract void Reset();
        public virtual void Enable() { }
        public virtual void Disable() { }
        public abstract void DeInit();
    }
}